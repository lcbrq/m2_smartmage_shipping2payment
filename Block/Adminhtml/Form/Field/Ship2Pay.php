<?php

namespace Smartmage\Shipping2Payment\Block\Adminhtml\Form\Field;

use Magento\Framework\DataObject;
use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

/**
 * Class Ship2Pay
 */
class Ship2Pay extends AbstractFieldArray
{
    /**
     * @var Shippings
     */
    protected $shippingRenderer = null;

    /**
     * @var Payments
     */
    protected $paymentRenderer = null;
    
    /**
     * Returns renderer for shipping element
     * 
     * @return Shippings
     */
    protected function getShippingRenderer()
    {
        if (!$this->shippingRenderer) {
            $this->shippingRenderer = $this->getLayout()->createBlock(
                Shippings::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->shippingRenderer;
    }

    /**
     * Returns renderer for payment element
     * 
     * @return Payments
     */
    protected function getPaymentRenderer()
    {
        if (!$this->paymentRenderer) {
            $this->paymentRenderer = $this->getLayout()->createBlock(
                Payments::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->paymentRenderer;
    }

    /**
     * Prepare to render
     * @return void
     */
    protected function _prepareToRender()
    {
        $this->addColumn(
            'shipping_method',
            [
                'label'     => __('Shipping Method'),
                'renderer'  => $this->getShippingRenderer(),
            ]
        );
        $this->addColumn(
            'payment_method',
            [
                'label' => __('Payment Method'),
                'renderer'  => $this->getPaymentRenderer(),
            ]
        );
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add Rule');
    }

    /**
     * Prepare existing row data object
     *
     * @param DataObject $row
     * @return void
     */
    protected function _prepareArrayRow(DataObject $row)
    {
        $shipping = $row->getShippingMethod();
        $options = [];
        if ($shipping) {
            $options['option_' . $this->getShippingRenderer()->calcOptionHash($shipping)]
                = 'selected="selected"';

            $payments = $row->getPaymentMethod();
            foreach ($payments as $payment) {
                $options['option_' . $this->getPaymentRenderer()->calcOptionHash($payment)]
                    = 'selected="selected"';
            }
        }
        $row->setData('option_extra_attrs', $options);
    }
}
