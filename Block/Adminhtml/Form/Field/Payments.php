<?php

namespace Smartmage\Shipping2Payment\Block\Adminhtml\Form\Field;

use Smartmage\Shipping2Payment\Helper\Payment;
use Magento\Framework\View\Element\Context;
use Magento\Framework\View\Element\Html\Select;

/**
 * Class Payments
 */
class Payments extends Select
{
    /**
     * @var Payment
     */
    private $paymentHelper;

    /**
     * Constructor
     *
     * @param Context $context
     * @param Payment $paymentHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Payment $paymentHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->paymentHelper = $paymentHelper;
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->getOptions()) {
            $this->setOptions($this->paymentHelper->getPaymentMethods());
        }
        $this->setClass('payment-method-select payment-multiselect');
        $this->setExtraParams('multiple="multiple"');
        return parent::_toHtml();
    }

    /**
     * Sets name for input element
     *
     * @param string $value
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value . '[]');
    }
}
