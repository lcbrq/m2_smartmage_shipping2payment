# README #

### SmartMage Shipping2Payment ###

Moduł do zarządzania zależnościami między dostawą a płatnością w procesie zamówienia.
Pozwala ukryć nie aktywne metody płatności od wybranych metod dostawy. Np: jeśli wybierzemy metodę "Kurier UPS przedpłata" w kroku "Dostawa" to w kroku "Podsumowanie i płatność" nie pojawi się opcja "Za pobraniem".

### Instalacja ###

Pliki moduły wgrywamy do katalogu /app/code/Smartmage/Shipping2Payment

Następnie w konsoli uruchamiamy następujące komendy:

* php bin/magento module:enable Smartmage_Shipping2Payment
* php bin/magento setup:upgrade
* php bin/magento setup:di:compile
* php bin/magento setup:static-content:deploy pl_PL
* php bin/magento cache:flush

Działanie modułu można przetestować na naszej wersji demonstracyjnej dostępnej pod adresem http://demo.m2.smartmage.pl/