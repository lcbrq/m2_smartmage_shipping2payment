<?php

namespace Smartmage\Shipping2Payment\Model\Checks;

use Magento\Payment\Model\MethodInterface;
use Magento\Quote\Model\Quote;

class Composite extends \Magento\Payment\Model\Checks\Composite
{
    /**
     * Check whether payment method is applicable to quote
     *
     * @param MethodInterface $paymentMethod
     * @param \Magento\Quote\Model\Quote $quote
     * @return bool
     */
    public function isApplicable(MethodInterface $paymentMethod, Quote $quote)
    {
        $parent_check = parent::isApplicable($paymentMethod, $quote);

        if(!$this->getShip2PayHelper()->isActive()){
        	return $parent_check;
		}

        if($parent_check){
            $ship2pay = $this->getShip2PayArray();
            $s2pCheck = false;
            if($quote->getShippingAddress()->getShippingMethod() && is_array($ship2pay)){
                $shipping_method = $quote->getShippingAddress()->getShippingMethod();
                $s2pCheck = true;
            }
            if($s2pCheck && isset($ship2pay[$shipping_method]) && is_array($ship2pay[$shipping_method]) && in_array($paymentMethod->getCode(), $ship2pay[$shipping_method])){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

	public function getShip2PayHelper()
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$ship2pay_helper = $objectManager->get('Smartmage\Shipping2Payment\Helper\Data');
		return $ship2pay_helper;
	}

    public function getShip2PayArray()
    {
		$ship2pay_helper = $this->getShip2PayHelper();
        $ship2pay = $ship2pay_helper->getShip2PayArray();
        return $ship2pay;
    }
}