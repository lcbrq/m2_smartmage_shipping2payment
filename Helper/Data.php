<?php

namespace Smartmage\Shipping2Payment\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Data
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Core store config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    public function __construct(
        StoreManagerInterface $storeManager,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_storeManager = $storeManager;
    }

    public function getShip2PayArray()
    {
        $ship2pay = unserialize($this->_scopeConfig->getValue(
            'ship2pay/settings/ship2pay',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->_storeManager->getStore()->getId())
        );

        return is_array($ship2pay) ? $ship2pay : [];
    }

	public function isActive()
	{
		$ship2payActive = $this->_scopeConfig->getValue(
			'ship2pay/settings/active',
			\Magento\Store\Model\ScopeInterface::SCOPE_STORE,
			$this->_storeManager->getStore()->getId());

		return $ship2payActive;
	}
}