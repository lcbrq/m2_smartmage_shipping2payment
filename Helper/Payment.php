<?php

namespace Smartmage\Shipping2Payment\Helper;

use Magento\Payment\Model\Config\Source\Allmethods as Allmethods;

/**
 * Class Payment
 */
class Payment extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * All possible payment method
     *
     * @var array
     */
    private $paymentMethod = [];

    /**
     * @var \Magento\Payment\Model\Config\Source\Allmethods
     */
    private $allmethods;

    /**
     * @param Allmethods $allmethods
     */
    public function __construct(Allmethods $allmethods)
    {
        $this->allmethods = $allmethods;
    }

    /**
     * All possible payment method
     *
     * @return array
     */
    public function getPaymentMethods()
    {
        if (!$this->paymentMethod) {
            $this->paymentMethod = $this->allmethods->toOptionArray();
        }
        return $this->paymentMethod;
    }
}